function goscr
    if set -q VISUAL || set -q EDITOR
        set -f editor (if set -q VISUAL; echo $VISUAL; else; echo $EDITOR; end)
        set -f tempfile ./goscr_tempfile.go

        if ! set -q _goscr_template
            set _goscr_template 'package main

import "fmt"
func main() {
    #
}'
        end

        echo $_goscr_template >$tempfile
        set -f line (grep '#' $tempfile)
        set -f row (grep -n $line $tempfile | cut -d: -f1)
        set -f col (echo $line | grep -ob '#' - | cut -d: -f1)
        set -f col (math $col + 1)

        while $editor $tempfile:$row:$col
            go run $tempfile

            read -lP 'Once again? ' retry
            switch $retry
                case NO No no n
                    break
            end
        end
        rm $tempfile
    end
end
